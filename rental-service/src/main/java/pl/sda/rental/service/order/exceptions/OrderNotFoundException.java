package pl.sda.rental.service.order.exceptions;

public class OrderNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -4627867888728589584L;

    public OrderNotFoundException() {
    }

    public OrderNotFoundException(String message) {
    super(message);
    }
}
