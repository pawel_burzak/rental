package pl.sda.rental.service.account.exceptions;

public class AccountNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 8406893557941640445L;

    public AccountNotFoundException() {
    }

    public AccountNotFoundException(String message) {
        super(message);
    }
}
