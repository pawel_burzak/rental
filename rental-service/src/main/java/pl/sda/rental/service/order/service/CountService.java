package pl.sda.rental.service.order.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.rental.domain.entity.Rent;
import pl.sda.rental.hibernate.repository.RentRepository;
import pl.sda.rental.service.order.util.DayPriceCounter;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Service
@Transactional
public class CountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CountService.class);

    private final RentRepository rentRepository;

    @Autowired
    public CountService(RentRepository rentRepository) {
        this.rentRepository = rentRepository;
    }

    public BigDecimal countDayPrice(Rent rent) {

        DayPriceCounter dayPriceCounter = new DayPriceCounter();
        BigDecimal price = dayPriceCounter.countDayPrice(rent);

        return price;
    }

    public BigDecimal countPayments(Long id) {
        LOGGER.debug("is executed");

        List<Rent> rents = rentRepository.findRentsByAccountId(id);

        BigDecimal payment = new BigDecimal(0);

        for (Rent rent : rents) {
            if (Objects.nonNull(rent.getPrice())) {
                payment = payment.add(rent.getPrice());
            }
        }

        return payment;
    }

    public BigDecimal countAllPayments() {
        LOGGER.debug("is executed");

        List<Rent> allRents = rentRepository.findAll();
        BigDecimal payment = new BigDecimal(0);

        for (Rent rent : allRents) {
            if (Objects.nonNull(rent.getPrice())) {
                payment = payment.add(rent.getPrice());
            }
        }
        return payment;
    }
}
