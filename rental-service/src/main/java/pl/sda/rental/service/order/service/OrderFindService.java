package pl.sda.rental.service.order.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.rental.domain.entity.Account;
import pl.sda.rental.domain.entity.Rent;
import pl.sda.rental.hibernate.repository.RentRepository;
import pl.sda.rental.service.order.exceptions.OrderNotFoundException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@Transactional
public class OrderFindService implements Serializable {
    private static final long serialVersionUID = 7667329648669311154L;

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderFindService.class);

    private final RentRepository rentRepository;

    @Autowired
    public OrderFindService(RentRepository rentRepository) {
        this.rentRepository = rentRepository;
    }

    public List<Rent> findRentsByAccountId(Long id) {
        return rentRepository.findRentsByAccountId(id);
    }

    public List<Account> findAllOrderAccounts() {
        List<Account> accounts = rentRepository.findAccountsWhoRent();
        accounts.sort(Comparator.comparing(Account::getLastName));
        return accounts;
    }
}
