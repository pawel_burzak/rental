package pl.sda.rental.service.account.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.rental.domain.entity.Account;
import pl.sda.rental.hibernate.repository.AccountRepository;
import pl.sda.rental.service.account.exceptions.AccountNotFoundException;

import java.util.List;

@Service
@Transactional
public class AccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountService.class);

    private final AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Long create(Account account) {
        LOGGER.debug("Create account");

        accountRepository.save(account);

        return account.getId();
    }

    public void update(Account account) {
        LOGGER.debug("Update account");

        Account findAccount = accountRepository.findOne(account.getId());

        if (findAccount == null) {
            LOGGER.debug("Account with id " + findAccount.getId() + " not found.");
            throw new AccountNotFoundException("Exception.message.accountNotFound");
        }

        setAccount(account, findAccount);

    }

    private void setAccount(Account account, Account findAccount) {
        findAccount.setFirstName(account.getFirstName());
        findAccount.setLastName(account.getLastName());
        findAccount.setPhoneNumber(account.getPhoneNumber());
        findAccount.setEmail(account.getEmail());
        findAccount.getAddress().setPostalCode(account.getAddress().getPostalCode());
        findAccount.getAddress().setCity(account.getAddress().getCity());
        findAccount.getAddress().setStreetAddress(account.getAddress().getStreetAddress());
    }

    public void delete(Long id) {
        LOGGER.debug("Delete account");

        Account findAccount = accountRepository.findOne(id);

        if (findAccount == null) {
            LOGGER.debug("Account with id " + findAccount.getId() + " not found.");
            throw new AccountNotFoundException("Exception.message.accountNotFound");
        }

        accountRepository.delete(findAccount);
    }

    public Account findByPhoneNumber(String phoneNumber) {
        LOGGER.debug("Find account");

        Account account = accountRepository.findByPhoneNumber(phoneNumber);

        if (account == null) {
            LOGGER.debug("Account with phone number " + phoneNumber + " not found.");
            throw new AccountNotFoundException("Exception.message.numberNotFound");
        }

        return account;
    }

    public List<Account> findByFirstAndLastName(String firstName, String lastName) {
        LOGGER.debug("Find accounts");

        List<Account> foundAccounts = accountRepository.findByFirstAndLastName(firstName, lastName);

        return foundAccounts;
    }

    public Account findById(Long id) {
        LOGGER.debug("Find account");

        Account account = accountRepository.findOne(id);

        if (account == null) {
            LOGGER.debug("Account with id " + id + " not found.");
            throw new AccountNotFoundException("Exception.message.accountNotFound");
        }

        return account;
    }

}
