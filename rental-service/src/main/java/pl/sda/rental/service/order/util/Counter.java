package pl.sda.rental.service.order.util;

import pl.sda.rental.domain.entity.Rent;

import java.math.BigDecimal;

public interface Counter {

    BigDecimal countDayPrice(Rent rent);
}
