package pl.sda.rental.service.order.util;

import pl.sda.rental.domain.entity.Rent;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;


public class DayPriceCounter implements Counter {

    @Override
    public BigDecimal countDayPrice(Rent rent) {

        BigDecimal price = new BigDecimal(0);

        BigDecimal time = BigDecimal.valueOf((Duration.between(rent.getRentStart(), rent.getRentEnd()).toMinutes()));

        if (time.intValue() < 240) {
            price = price.add((time.divide(new BigDecimal(60), 2, RoundingMode.HALF_EVEN).multiply(rent.getEquipment().getPricePerHour())));
        } else {
            price = price.add(rent.getEquipment().getPricePerDay());
    }

        return price;
    }
}
