package pl.sda.rental.service.equipment.exceptions;

public class EquipmentNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -6664781539074015368L;

    public EquipmentNotFoundException() {
    }

    public EquipmentNotFoundException(String message) {
        super(message);
    }
}
