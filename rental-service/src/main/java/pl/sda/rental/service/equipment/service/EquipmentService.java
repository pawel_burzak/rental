package pl.sda.rental.service.equipment.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.rental.domain.entity.Equipment;
import pl.sda.rental.hibernate.repository.EquipmentRepository;
import pl.sda.rental.service.equipment.exceptions.EquipmentNotFoundException;

import java.util.List;

@Service
@Transactional
public class EquipmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EquipmentService.class);

    private final EquipmentRepository equipmentRepository;

    @Autowired
    public EquipmentService(EquipmentRepository equipmentRepository) {
        this.equipmentRepository = equipmentRepository;
    }

    public Long create(Equipment equipment) {
        LOGGER.debug("Create equipment");

        equipmentRepository.save(equipment);

        return equipment.getId();
    }

    public List<Equipment> findAll() {
        return equipmentRepository.findAll();
    }

    public Equipment findById(Long id) {
        LOGGER.debug("Find equipment");

        Equipment equipment = equipmentRepository.findOne(id);

        if (equipment == null) {
            LOGGER.debug("Equipment with id " + id + " not found.");
            throw new EquipmentNotFoundException("Exception.message.equipmentNotFound");
        }

        return equipment;
    }

    public void update(Equipment equipment) {
        LOGGER.debug("Update equipment");

        Equipment findEquipment = equipmentRepository.findOne(equipment.getId());

        if (findEquipment == null) {
            LOGGER.debug("Equipment with id " + findEquipment.getId() + " not found.");
            throw new EquipmentNotFoundException("Exception.message.equipmentNotFound");
        }

        setEquipment(equipment, findEquipment);
    }

    private void setEquipment(Equipment equipment, Equipment findEquipment) {
        findEquipment.setName(equipment.getName());
        findEquipment.setPricePerHour(equipment.getPricePerHour());
        findEquipment.setPricePerDay(equipment.getPricePerDay());
    }

    public void delete(Long id) {
        LOGGER.debug("Delete equipment");

        Equipment findEquipment = equipmentRepository.findOne(id);

        if (findEquipment == null) {
            LOGGER.debug("Equipment with id " + findEquipment.getId() + " not found.");
            throw new EquipmentNotFoundException("Exception.message.equipmentNotFound");
        }

        equipmentRepository.delete(id);
    }
}
