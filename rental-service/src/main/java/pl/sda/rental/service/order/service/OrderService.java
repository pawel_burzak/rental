package pl.sda.rental.service.order.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.rental.domain.dto.RentDTO;
import pl.sda.rental.domain.entity.Account;
import pl.sda.rental.domain.entity.Equipment;
import pl.sda.rental.domain.entity.Rent;
import pl.sda.rental.domain.wrapper.Order;
import pl.sda.rental.hibernate.repository.AccountRepository;
import pl.sda.rental.hibernate.repository.EquipmentRepository;
import pl.sda.rental.hibernate.repository.RentRepository;
import pl.sda.rental.service.order.exceptions.OrderNotFoundException;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

@Service
@Transactional
public class OrderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

    private final RentRepository rentRepository;
    private final AccountRepository accountRepository;
    private final EquipmentRepository equipmentRepository;
    private final CountService countService;

    @Autowired
    public OrderService(RentRepository rentRepository, AccountRepository accountRepository, EquipmentRepository equipmentRepository, CountService countService) {
        this.rentRepository = rentRepository;
        this.accountRepository = accountRepository;
        this.equipmentRepository = equipmentRepository;
        this.countService = countService;
    }

    public Long createOrder(final Order order) {
        LOGGER.debug("Create order");

        final Account account = accountRepository.findOne(order.getAccountId());
        order.getRentsDTO().stream()
                .map(rentDTO -> processRent(account, order.getRentStart(), rentDTO))
                .forEach(rentRepository::save);

        return order.getAccountId();
    }

    private List<Rent> processRent(Account account, LocalTime rentStart, RentDTO rentDTO) {
        Equipment equipment = equipmentRepository.findOne(rentDTO.getEquipmentId());

        return LongStream.range(0, rentDTO.getAmount())
                .mapToObj(index -> new Rent(account, equipment, rentStart))
                .collect(Collectors.toList());
    }

    public void updateOrder(Order order) {
        LOGGER.debug("Update order");

        List<Rent> findRents = rentRepository.findRentsByAccountId(order.getAccountId());

        if (findRents.isEmpty()) {
            LOGGER.debug("No rents for account with id " + order.getAccountId() + ".");
            throw new OrderNotFoundException("Exception.message.orderNotFound");
        }

        for (Rent rent : findRents) {
            rent.setRentEnd(findRentFromOrderById(rent.getId(), order).get().getRentEnd());
            if (Objects.nonNull(rent.getRentEnd())) {
                BigDecimal price = countService.countDayPrice(rent);
                rent.setPrice(price);
            }
        }
    }

    private Optional<Rent> findRentFromOrderById(Long id, Order order) {
        return order.getRents().stream().filter(rent -> rent.getId().equals(id)).findAny();
    }

    public void deleteOrder(Long id) {
        LOGGER.debug("Delete order");

        List<Rent> rents = rentRepository.findRentsByAccountId(id);

        if (rents.isEmpty()) {
            LOGGER.debug("No rents for account with id " + id + ".");
            throw new OrderNotFoundException("Exception.message.orderNotFound");
        }

        for (Rent rent : rents) {
            rentRepository.delete(rent);
        }
    }

    public void deleteRent(Long id) {
        LOGGER.debug("is executed");

        Rent rent = rentRepository.findOne(id);

        if (rent == null) {
            LOGGER.debug("Rent with id " + id + " not found.");
            throw new OrderNotFoundException("Exception.message.orderNotFound");
        }

        rentRepository.delete(rent);

    }

    public void deletePaidOrders() {
        List<Rent> all = rentRepository.findAll();
        LOGGER.debug("is executed!");

        if (all.isEmpty()) {
            LOGGER.debug("List of orders is empty.");
            throw new OrderNotFoundException("Exception.message.orderNotFound");
        }

        for (Rent rent : all) {
            if (Objects.nonNull(rent.getPrice())) {
                rentRepository.delete(rent);
            }
        }
    }

}
