package pl.sda.rental.domain.entity;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Table(name = "ACCOUNT")
@Entity
@AttributeOverride(name = "id", column = @Column(name = "ACCOUNT_ID"))
public class Account extends BaseEntity implements Serializable{

    private static final long serialVersionUID = 2574477766255204928L;

    @NotBlank
    @Length(min = 3)
    @Column(name = "FIRST_NAME")
    private String firstName;

    @NotBlank
    @Column(name = "LAST_NAME")
    private String lastName;

    @NotBlank
    @Pattern(regexp="(^$|[0-9]{9})")
    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Email
    @Column(name = "EMAIL")
    private String email;

    @Valid
    @Embedded
    private Address address;

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Rent> rents = new HashSet<>();

    public Account() {
    }

    public Account(String firstName, String lastName, String phoneNumber, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<Rent> getRents() {
        return rents;
    }

    public void setRents(Set<Rent> rents) {
        this.rents = rents;
    }

    @Override
    public String toString() {
        return "Klient: " + firstName + " " + lastName + ", numer telefonu: " +
                phoneNumber + ", adres email: " + email +
                ", adres: " + address.getStreetAddress() + ", " +
                address.getPostalCode() + " " + address.getCity();
    }

}
