package pl.sda.rental.domain.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class RentDTO implements Serializable {
    private static final long serialVersionUID = 6117450744057099265L;

    private Long equipmentId;

    private String equipmentName;

    @NotNull
    @Min(value = 0)
    @Max(value = 10)
    private Long amount;

    public RentDTO() {
    }

    public RentDTO(Long equipmentId, String equipmentName) {
        this.equipmentId = equipmentId;
        this.equipmentName = equipmentName;
    }

    public Long getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(Long equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

}
