package pl.sda.rental.domain.entity;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Pattern;

@Embeddable
public class Address {

    @Pattern(regexp="(^$|[0-9]{2})-(^$|[0-9]{3})")
    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @NotBlank
    @Column(name = "CITY")
    private String city;

    @NotBlank
    @Column(name = "STREET_ADDRESS")
    private String streetAddress;

    public Address() {
    }

    public Address(String postalCode, String city, String streetAddress) {
        this.postalCode = postalCode;
        this.city = city;
        this.streetAddress = streetAddress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }
}
