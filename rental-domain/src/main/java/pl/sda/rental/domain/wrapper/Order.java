package pl.sda.rental.domain.wrapper;

import pl.sda.rental.domain.dto.RentDTO;
import pl.sda.rental.domain.entity.Rent;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;


public class Order {

    @Valid
    private List<RentDTO> rentsDTO = new ArrayList<>();

    private List<Rent> rents;

    @NotNull
    private LocalTime rentStart;

    private Long accountId;

    public Order() {
    }

    public List<RentDTO> getRentsDTO() {
        return rentsDTO;
    }

    public void setRentsDTO(List<RentDTO> rentsDTO) {
        this.rentsDTO = rentsDTO;
    }

    public LocalTime getRentStart() {
        return rentStart;
    }

    public void setRentStart(LocalTime rentStart) {
        this.rentStart = rentStart;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public List<Rent> getRents() {
        return rents;
    }

    public void setRents(List<Rent> rents) {
        this.rents = rents;
    }
}
