package pl.sda.rental.domain.entity;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Table(name = "EQUIPMENT")
@Entity
@AttributeOverride(name = "id", column = @Column(name = "EQUIPMENT_ID"))
public class Equipment extends BaseEntity implements Serializable{

    private static final long serialVersionUID = -7521326518200146268L;

    @NotBlank
    @Column(name = "NAME")
    private String name;

    @NotNull
    @DecimalMin("5.00")
    @DecimalMax("100.00")
    @Column(name = "PRICE_PER_HOUR")
    private BigDecimal pricePerHour;

    @NotNull
    @DecimalMin("10.00")
    @DecimalMax("300.00")
    @Column(name = "PRICE_PER_DAY")
    private BigDecimal pricePerDay;

    @OneToMany(mappedBy = "equipment", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Rent> rents = new HashSet<>();

    public Equipment() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(BigDecimal pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public BigDecimal getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(BigDecimal pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public Set<Rent> getRents() {
        return rents;
    }

    public void setRents(Set<Rent> rents) {
        this.rents = rents;
    }

    @Override
    public String toString() {
        return getName();
    }
}
