package pl.sda.rental.domain.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

@Table(name = "RENT")
@Entity
@AttributeOverride(name = "id", column = @Column(name = "RENT_ID"))
public class Rent extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 3427766353536651663L;

    @ManyToOne
    @JoinColumn(name = "ACCOUNT_ID", nullable = false, foreignKey = @ForeignKey(name = "FK_RENT_ACCOUNT"))
    private Account account;

    @ManyToOne
    @JoinColumn(name = "EQUIPMENT_ID", nullable = false, foreignKey = @ForeignKey(name = "FK_RENT_EQUIPMENT"))
    private Equipment equipment;

    @Column(name="RENT_START")
    private LocalTime rentStart;

    @Column(name="RENT_END")
    private LocalTime rentEnd;

    @Column(name = "PRICE")
    private BigDecimal price;

    public Rent() {
    }

    public Rent(Account account, Equipment equipment, LocalTime rentStart) {
        this.account = account;
        this.equipment = equipment;
        this.rentStart = rentStart;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public LocalTime getRentStart() {
        return rentStart;
    }

    public void setRentStart(LocalTime rentStart) {
        this.rentStart = rentStart;
    }

    public LocalTime getRentEnd() {
        return rentEnd;
    }

    public void setRentEnd(LocalTime rentEnd) {
        this.rentEnd = rentEnd;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

}
