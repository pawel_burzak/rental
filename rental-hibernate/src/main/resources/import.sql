insert into account (LATEST_VERSION, CITY, POSTAL_CODE, STREET_ADDRESS, EMAIL, FIRST_NAME, LAST_NAME, PHONE_NUMBER) values ('0', 'Lublin', '20-538', 'Jutrzenki 16/94', 'b.p@o2.pl', 'Adam', 'Kowalski', '444444444');
insert into account (LATEST_VERSION, CITY, POSTAL_CODE, STREET_ADDRESS, EMAIL, FIRST_NAME, LAST_NAME, PHONE_NUMBER) values ('0', 'Warszawa', '00-300', 'Marszałkowska 22', 'k.m@o2.pl', 'Karol', 'Marcinkowski', '111111111');
insert into account (LATEST_VERSION, CITY, POSTAL_CODE, STREET_ADDRESS, EMAIL, FIRST_NAME, LAST_NAME, PHONE_NUMBER) values ('0', 'Gdańsk', '80-088', 'Juranda 4', 'm.b@o2.pl', 'Mikołaj', 'Berc', '222222222');
insert into account (LATEST_VERSION, CITY, POSTAL_CODE, STREET_ADDRESS, EMAIL, FIRST_NAME, LAST_NAME, PHONE_NUMBER) values ('0', 'Poznań', '60-102', 'Diamentowa 84/94', 'e.k@o2.pl', 'Ewa', 'Kotwicka', '333333333');

insert into equipment (LATEST_VERSION, NAME, PRICE_PER_DAY, PRICE_PER_HOUR) values ('0', 'Komplet początkujący', '140', '35');
insert into equipment (LATEST_VERSION, NAME, PRICE_PER_DAY, PRICE_PER_HOUR) values ('0', 'Komplet zaawansowany', '200', '50');
insert into equipment (LATEST_VERSION, NAME, PRICE_PER_DAY, PRICE_PER_HOUR) values ('0', 'Pianka', '20', '5');
insert into equipment (LATEST_VERSION, NAME, PRICE_PER_DAY, PRICE_PER_HOUR) values ('0', 'Trapez', '20', '5');
insert into equipment (LATEST_VERSION, NAME, PRICE_PER_DAY, PRICE_PER_HOUR) values ('0', 'Ubezpieczenie', '20', '5');
insert into equipment (LATEST_VERSION, NAME, PRICE_PER_DAY, PRICE_PER_HOUR) values ('0', 'Żagiel', '80', '20');
insert into equipment (LATEST_VERSION, NAME, PRICE_PER_DAY, PRICE_PER_HOUR) values ('0', 'Deska X-Cite Ride', '140', '35');
insert into equipment (LATEST_VERSION, NAME, PRICE_PER_DAY, PRICE_PER_HOUR) values ('0', 'Deska All-Ride', '160', '45');
insert into equipment (LATEST_VERSION, NAME, PRICE_PER_DAY, PRICE_PER_HOUR) values ('0', 'Deska Super-Sport', '220', '55');
insert into equipment (LATEST_VERSION, NAME, PRICE_PER_DAY, PRICE_PER_HOUR) values ('0', 'Deska Freestyle-Wave', '220', '55');