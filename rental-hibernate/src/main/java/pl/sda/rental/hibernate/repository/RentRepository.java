package pl.sda.rental.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.rental.domain.entity.Account;
import pl.sda.rental.domain.entity.Rent;

import java.util.List;

@Repository
public interface RentRepository extends JpaRepository<Rent, Long> {

    @Query("SELECT p FROM Rent p WHERE p.account.id = :id")
    List<Rent> findRentsByAccountId(@Param("id") Long id);

    @Query("SELECT distinct account FROM Rent")
    List<Account> findAccountsWhoRent();
}
