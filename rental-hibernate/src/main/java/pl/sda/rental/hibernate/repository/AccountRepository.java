package pl.sda.rental.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.rental.domain.entity.Account;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query("SELECT p FROM Account p WHERE p.phoneNumber = :phoneNumber")
    Account findByPhoneNumber(@Param("phoneNumber") String phoneNumber);

    @Query("SELECT p FROM Account p WHERE p.firstName = :firstName AND p.lastName = :lastName")
    List<Account> findByFirstAndLastName(@Param("firstName")String firstName, @Param("lastName") String lastName);

}
