# WINDSURFING RENTAL #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

#### This application helps to calculate price of rent. It provides: ####
* whole CRUD of account using database
* whole CRUD of equipment using database
* create, read and delete rent using database
* delete paid orders
* find account by first and last name to edit account
* find account by phone number to rent windsurfing
* rent windsurfing
* calculate price of single rent, whole order and all orders

### How do I get set up? ###

#### You need: ####
* JDK 1.8 or later
* Maven 3 or later
* MySQL Server 5.7 or later
* Tomcat 8.5 or later

#### Dependencies: ####
* Spring MVC, Spring Security, Spring Data JPA
* Hibernate Core, Hibernate JPA, Hibernate Validator
* Javax Servlet Api
* JSTL
* DBCP2
* MySQL Connector
* SLF4J
* Logback
* JUnit
    
#### Database configuration: ####
* create schema in MySQL called: rental
* create mysql username: rental
* create mysql password: rental

#### Deployment instructions: ####
* login: funsurf
* password: funsurf