package pl.sda.rental.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.rental.domain.dto.AccountSearchDTO;
import pl.sda.rental.domain.dto.RentDTO;
import pl.sda.rental.domain.entity.Account;
import pl.sda.rental.domain.wrapper.Order;
import pl.sda.rental.service.account.exceptions.AccountNotFoundException;
import pl.sda.rental.service.account.service.AccountService;
import pl.sda.rental.service.equipment.service.EquipmentService;
import pl.sda.rental.service.order.service.OrderFindService;
import pl.sda.rental.service.order.service.OrderService;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Controller
public class RentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RentController.class);

    private final EquipmentService equipmentService;
    private final AccountService accountService;
    private final OrderService orderService;
    private final OrderFindService orderFindService;
    private final MessageSource messageSource;

    @Autowired
    public RentController(EquipmentService equipmentService,
                          AccountService accountService,
                          OrderService orderService,
                          OrderFindService orderFindService, MessageSource messageSource) {
        this.equipmentService = equipmentService;
        this.accountService = accountService;
        this.orderService = orderService;
        this.orderFindService = orderFindService;
        this.messageSource = messageSource;
    }

    @RequestMapping(value = "/rent", method = RequestMethod.GET)
    public ModelAndView main(@RequestParam(required = false) String failure, Locale locale,
                             @ModelAttribute("flashAccount") AccountSearchDTO account,
                             @ModelAttribute("flashOrder") Order order) {
        LOGGER.debug("is executed!");

        ModelAndView model = new ModelAndView();

        if (failure != null) {
            model.addObject("failure",
                    messageSource.getMessage("message.failure", new String[]{}, locale));
        }

        model.setViewName("rent");

        model.addObject("foundAccount", account);
        model.addObject("order", order);


        return model;
    }


    @RequestMapping(value = "/rent/findAccount", method = RequestMethod.POST)
    public String findAccount(Model model, @Valid @ModelAttribute("foundAccount") AccountSearchDTO account,
                              BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOGGER.debug("is executed!");

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.foundAccount", bindingResult);
            redirectAttributes.addFlashAttribute("flashAccount", account);

            return "redirect:/rent";
        }

        Account dbAccount;
        try {
            dbAccount = accountService.findByPhoneNumber(account.getPhoneNumber());
        } catch (AccountNotFoundException e) {

            bindingResult.rejectValue("phoneNumber", e.getMessage());

            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.foundAccount", bindingResult);
            redirectAttributes.addFlashAttribute("flashAccount", account);

            return "redirect:/rent";
        }

        List<RentDTO> rents = equipmentService.findAll().stream()
                .map(equipment -> new RentDTO(equipment.getId(), equipment.getName()))
                .collect(Collectors.toList());

        Order order = new Order();
        order.setRentsDTO(rents);
        order.setAccountId(dbAccount.getId());

        model.addAttribute("account", dbAccount);
        model.addAttribute("order", order);

        return "rent";
    }

    @RequestMapping(value = "/rent/saveRent", method = RequestMethod.POST)
    public String saveOrder(@Valid @ModelAttribute("order") Order order,
                            BindingResult bindingResult) {
        LOGGER.debug("is executed!");

        if (bindingResult.hasErrors()) {
            return "redirect:/rent?failure";
        }

        orderService.createOrder(order);
        return "redirect:/main?success";
    }

    @RequestMapping(value = "/rent/delete/{id}", method = RequestMethod.GET)
    public String deleteRentFromOrder(@PathVariable("id") Long id) {
        LOGGER.debug("is executed!");

        orderService.deleteRent(id);

        return "redirect:/payment?success";
    }


}
