package pl.sda.rental.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.rental.domain.entity.Account;
import pl.sda.rental.domain.entity.Rent;
import pl.sda.rental.domain.wrapper.Order;
import pl.sda.rental.service.account.service.AccountService;
import pl.sda.rental.service.order.service.CountService;
import pl.sda.rental.service.order.service.OrderFindService;
import pl.sda.rental.service.order.service.OrderService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

@Controller
public class PaymentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentController.class);

    private final OrderService orderService;
    private final OrderFindService orderFindService;
    private final CountService countService;
    private final MessageSource messageSource;
    private final AccountService accountService;

    @Autowired
    public PaymentController(OrderService orderService, OrderFindService orderFindService, CountService countService, MessageSource messageSource, AccountService accountService) {
        this.orderService = orderService;
        this.orderFindService = orderFindService;
        this.countService = countService;
        this.messageSource = messageSource;
        this.accountService = accountService;
    }

    @RequestMapping(value = "/payment", method = RequestMethod.GET)
    public ModelAndView main(@RequestParam(required = false) String success, Locale locale) {
        LOGGER.debug("is executed!");

        ModelAndView model = new ModelAndView();

        if (success != null) {
            model.addObject("success",
                    messageSource.getMessage("message.success", new String[]{}, locale));
        }

        model.addObject("orderAccountList", orderFindService.findAllOrderAccounts());
        model.addObject("allPayments", countService.countAllPayments());

        model.setViewName("payment");

        return model;
    }

    @RequestMapping(value = "/payment/showDetails/{id}", method = RequestMethod.GET)
    public String showDetails(@PathVariable("id") Long id, Model model) {
        LOGGER.debug("is executed!");

        Order order = setOrder(id);

        model.addAttribute("accountOrder", order);
        model.addAttribute("account", accountService.findById(id));
        model.addAttribute("payment", countService.countPayments(id));

        return "showDetails";

    }

    private Order setOrder(Long id) {
        List<Rent> rents = orderFindService.findRentsByAccountId(id);
        Order order = new Order();
        order.setAccountId(id);
        order.setRents(rents);

        return order;
    }

    @RequestMapping(value = "/payment/showDetails/{id}/update", method = RequestMethod.POST)
    public String save(@PathVariable("id") Long id, @ModelAttribute("accountOrder") Order order, Model model) {
        LOGGER.debug("is executed!");

        orderService.updateOrder(order);
        Order updatedOrder = setOrder(id);

        model.addAttribute("accountOrder", updatedOrder);
        model.addAttribute("account", accountService.findById(id));
        model.addAttribute("payment", countService.countPayments(id));

        return "showDetails";

    }

    @RequestMapping(value = "/payment/deleteOrder/{id}", method = RequestMethod.GET)
    public String deleteOrder(@PathVariable("id") Long id) {
        LOGGER.debug("is executed!");

        orderService.deleteOrder(id);

        return "redirect:/payment?success";
    }

    @RequestMapping(value = "/payment/deletePaidOrder", method = RequestMethod.GET)
    public String deletePaidOrders() {

        orderService.deletePaidOrders();

        return "redirect:/payment?success";
    }

}
