package pl.sda.rental.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.rental.domain.entity.Equipment;
import pl.sda.rental.service.equipment.service.EquipmentService;

import javax.validation.Valid;
import java.util.Locale;

@Controller
public class EquipmentController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EquipmentController.class);

    private final EquipmentService equipmentService;
    private final MessageSource messageSource;

    @Autowired
    public EquipmentController(EquipmentService equipmentService, MessageSource messageSource) {
        this.equipmentService = equipmentService;
        this.messageSource = messageSource;
    }

    @RequestMapping(value = "/equipment", method = RequestMethod.GET)
    public ModelAndView main(@RequestParam(required = false) String success, Locale locale,
                             @ModelAttribute("flashEquipment") Equipment equipment) {
        LOGGER.debug("is executed");

        ModelAndView model = new ModelAndView();

        if (success != null) {
            model.addObject("success",
                    messageSource.getMessage("message.success", new String[]{}, locale));
        }

        model.addObject("equipment", equipment);
        model.addObject("equipmentList", equipmentService.findAll());

        model.setViewName("equipment");

        return model;
    }

    @RequestMapping(value = "/equipment/save", method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute("equipment") Equipment equipment,
                       BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.equipment", bindingResult);
            redirectAttributes.addFlashAttribute("flashEquipment", equipment);

            return "redirect:/equipment";
        }

        if (equipment.getId() == null) {
            equipmentService.create(equipment);
        } else {
            equipmentService.update(equipment);
        }

        return "redirect:/equipment?success";
    }

    @RequestMapping(value = "/equipment/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") Long id, Model model) {
        LOGGER.debug("is executed!");

        model.addAttribute("equipment", equipmentService.findById(id));
        model.addAttribute("equipmentList", equipmentService.findAll());

        return "equipment";
    }

    @RequestMapping(value = "/equipment/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") Long id) {
        LOGGER.debug("is executed!");

        equipmentService.delete(id);

        return "redirect:/equipment?success";
    }


}
