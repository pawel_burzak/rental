package pl.sda.rental.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.sda.rental.domain.entity.Account;
import pl.sda.rental.service.account.service.AccountService;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

@Controller
public class AccountController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

    private final AccountService accountService;
    private final MessageSource messageSource;

    @Autowired
    public AccountController(AccountService accountService, MessageSource messageSource) {
        this.accountService = accountService;
        this.messageSource = messageSource;
    }

    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public ModelAndView index(@RequestParam(required = false) String success, Locale locale) {
        LOGGER.debug("is executed!");

        ModelAndView model = new ModelAndView();

        if (success != null) {
            model.addObject("success",
                    messageSource.getMessage("message.success", new String[]{}, locale));
        }

        model.setViewName("main");

        return model;
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public String main(@ModelAttribute("flashAccount") Account account, Model model) {
        LOGGER.debug("is executed!");

        model.addAttribute("account", account);
        model.addAttribute("findAccounts", new Account());

        return "newUser";

    }

    @RequestMapping(value = "/account/save", method = RequestMethod.POST)
    public String save(@Valid @ModelAttribute("account") Account account,
                       BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        LOGGER.debug("is executed!");

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.account", bindingResult);
            redirectAttributes.addFlashAttribute("flashAccount", account);

            return "redirect:/account";
        }

        if (account.getId() == null) {
            accountService.create(account);
        } else {
            accountService.update(account);
        }

        return "redirect:/main?success";
    }

    @RequestMapping(value = "/account/find", method = RequestMethod.POST)
    public String find(@ModelAttribute("findAccounts") Account account, Model model) {
        LOGGER.debug("is executed!");

        List<Account> foundAccounts = accountService.findByFirstAndLastName(account.getFirstName(), account.getLastName());

        model.addAttribute("foundAccounts", foundAccounts);
        model.addAttribute("account", new Account());

        return "newUser";

    }

    @RequestMapping(value = "/account/edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") Long id, Model model) {
        LOGGER.debug("is executed!");

        model.addAttribute("findAccounts", new Account());
        model.addAttribute("account", accountService.findById(id));

        return "newUser";
    }

    @RequestMapping(value = "/account/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable("id") Long id) {
        LOGGER.debug("is executed!");

        accountService.delete(id);

        return "redirect:/account";
    }


}
