package pl.sda.rental.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Locale;

@Controller
public class LoginController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    private final MessageSource messageSource;

    @Autowired
    public LoginController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(required = false) String error,
                              @RequestParam(required = false) String logout,
                              Locale locale) {
        LOGGER.debug("is executed!");

        ModelAndView modelAndView = new ModelAndView();

        if (error != null) {
            modelAndView.addObject("error",
                    messageSource.getMessage("login.message.error", new String[]{}, locale));
        }

        if (logout != null) {
            modelAndView.addObject("msg",
                    messageSource.getMessage("logout.message.success", new String[]{}, locale));
        }
        modelAndView.setViewName("login");

        return modelAndView;
    }
}
