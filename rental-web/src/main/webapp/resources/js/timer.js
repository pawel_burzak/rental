function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    h = checkTime(h);
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById("clock").innerHTML =
        h + ":" + m + ":" + s;
    setTimeout(startTime, 500);
}

function setTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes() + 10;
    h = checkTime(h);
    m = checkTime(m);
    document.getElementById("time").value =
        h + ":" + m;
}

function checkTime(i) {
    if (i < 10) {i = "0" + i};
    return i;
}

