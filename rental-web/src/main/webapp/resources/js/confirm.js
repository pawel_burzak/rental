function confirmDeleteAccount() {
    var x = confirm("Czy na pewno chcesz usunąć tego klienta?");
    if (x)
        return true;
    else
        return false;
}

function confirmDeleteEquipment() {
    var x = confirm("Czy na pewno chcesz usunąć tą pozycję?");
    if (x)
        return true;
    else
        return false;
}

function confirmDeletePaidOrders() {
    var x = confirm("Czy na pewno chcesz usunąć wszystkie opłacone wypożyczenia?");
    if (x)
        return true;
    else
        return false;
}

function confirmDeleteOrder() {
    var x = confirm("Czy na pewno chcesz usunąć to wypożyczenie?");
    if (x)
        return true;
    else
        return false;
}

