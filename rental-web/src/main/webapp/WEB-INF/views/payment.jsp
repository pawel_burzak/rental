<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <title>rental</title>

    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/js/timer.js" />"></script>
    <script src="<c:url value="/resources/js/confirm.js" />"></script>

</head>
<body onload="startTime()">

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="collapse navbar-collapse" id="navbarColor01">

        <c:if test="${not empty success}">
            <div class="alert alert-dismissible alert-success mr-auto">
                <strong>${success}</strong>
            </div>
        </c:if>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item" style="padding-right: 20px; font-size: 23px; font-weight: bold" id="clock"></li>
            <li class="nav-item navi_button">
                <a href="<c:url value='/main'/>">
                    <img type="button" class="home_button">
                </a>
            </li>
            <li class="nav-item navi_button">
                <c:url var="logoutUrl" value="/perform_logout"/>
                <form:form method="post" action="${logoutUrl}">
                    <input type="image" value="" class="logout_button"/>
                </form:form>
            </li>
        </ul>
    </div>
</nav>

<div class="display" style="margin-top: 100px">
    <c:out value="Dzienne rozliczenie: ${allPayments}"/>
    <br>
    <c:choose>
        <c:when test="${!empty orderAccountList}">
            <a href="<c:url value='/payment/deletePaidOrder'/>" onclick="return confirmDeletePaidOrders()">
                <spring:message code="payment.action.delete"/>
            </a>
        </c:when>
    </c:choose>
</div>

<div class="form_rent">
    <c:choose>
        <c:when test="${!empty orderAccountList}">
            <table class="table table-striped table-hover table-bordered">
                <thead class="thead-blue">
                <tr>
                    <th><spring:message code="label.firstAndLastName"/></th>
                    <th><spring:message code="label.phoneNumber"/></th>
                    <th colspan="2"></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${orderAccountList}" var="account">
                    <tr>
                        <td>${account.lastName} ${account.firstName}</td>
                        <td>${account.phoneNumber}</td>
                        <td>
                            <a href="<c:url value='/payment/showDetails/${account.id}'/>">
                                <spring:message code="action.show"/>
                            </a>
                        </td>
                        <td>
                            <a href="<c:url value='/payment/deleteOrder/${account.id}'/>" onclick="return confirmDeleteOrder()">
                                <spring:message code="action.delete"/>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:when>
        <c:otherwise>
            <div class="display"><spring:message code="order.list.message.empty"/></div>
        </c:otherwise>
    </c:choose>
</div>

</body>
</html>
