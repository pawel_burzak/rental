<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <title>rental</title>
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="collapse navbar-collapse" id="navbarColor01">
        <c:if test="${not empty msg}">
            <div class="alert alert-dismissible alert-success mr-auto">
                <strong>${msg}</strong>
            </div>
        </c:if>
        <c:if test="${not empty error}">
            <div class="alert alert-dismissible alert-danger mr-auto">
                <strong>${error}</strong>
            </div>
        </c:if>
    </div>
</nav>

<div class="form">
    <form:form action="perform_login" method="POST">
        <table>
            <tr>
                <td><spring:message code="login.label.user"/></td>
                <td>
                    <input class="input_form" type="text" name="username" placeholder="Nazwa użytkownika"
                           autofocus="true"/>
                </td>
            </tr>
            <tr>
                <td><spring:message code="login.label.password"/></td>
                <td>
                    <input class="input_form" type="password" placeholder="Hasło" name="password"/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <spring:message code="login.submit.label" var="labelSubmit"/>
                    <input class="input_form btn-primary" name="submit" type="submit" value="${labelSubmit}"/>
                </td>
            </tr>
        </table>
    </form:form>
</div>

</body>
</html>

