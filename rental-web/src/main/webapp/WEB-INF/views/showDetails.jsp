<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>rental</title>

    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/js/timer.js" />"></script>
    <script src="<c:url value="/resources/js/confirm.js" />"></script>

</head>
<body onload="startTime()">

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="collapse navbar-collapse" id="navbarColor01">

        <ul class="navbar-nav ml-auto">
            <li class="nav-item" style="padding-right: 20px; font-size: 23px; font-weight: bold" id="clock"></li>
            <li class="nav-item navi_button">
                <a href="<c:url value='/main'/>">
                    <img type="button" class="home_button">
                </a>
            </li>
            <li class="nav-item navi_button">
                <c:url var="logoutUrl" value="/perform_logout"/>
                <form:form method="post" action="${logoutUrl}">
                    <input type="image" value="" class="logout_button"/>
                </form:form>
            </li>
        </ul>
    </div>
</nav>


<div class="display" style="margin-top: 100px">
    <c:out value="Klient: ${account.firstName} ${account.lastName}, kwota do zapłaty: ${payment} zł"/>
</div>

<div class="form_rent">
    <c:choose>
        <c:when test="${!empty accountOrder.rents}">
            <c:url value="/payment/showDetails/${accountOrder.accountId}/update" var="updateAction"/>
            <form:form action="${updateAction}" modelAttribute="accountOrder" method="post">
                <table class="table table-striped table-hover table-bordered">
                    <thead class="thead-blue">
                    <tr>
                        <th rowspan="2" style="text-align:center;vertical-align:middle;padding:0"><spring:message code="equipment.label.name"/></th>
                        <th colspan="2">Godzina</th>
                        <th rowspan="2"></th>
                    </tr>
                    <tr>
                        <th>rozpoczęcia</th>
                        <th>zakończenia</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="rent" items="${accountOrder.rents}" varStatus="i">
                        <form:hidden path="accountId"/>
                        <input type="hidden" name="rents[${i.index}].id" value="${rent.id}">
                        <input type="hidden" name="rents[${i.index}].account.id" value="${rent.account.id}">
                        <tr>
                            <td>${rent.equipment.name}</td>
                            <td>${rent.rentStart}</td>
                            <c:choose>
                                <c:when test="${!empty rent.rentEnd}">
                                    <td>${rent.rentEnd}</td>
                                    <input type="hidden" name="rents[${i.index}].rentEnd" value="${rent.rentEnd}">
                                </c:when>
                                <c:otherwise>
                                    <td><input type="time" name="rents[${i.index}].rentEnd"/></td>
                                </c:otherwise>
                            </c:choose>
                            <td>
                                <a href="<c:url value='/rent/delete/${accountOrder.rents.get(i.index).id}'/>" onclick="return confirmDeleteOrder()">
                                    <spring:message code="action.delete"/>
                                </a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

                <table class="form_rent" style="width: 100px">
                    <tr>
                        <td>
                            <spring:message code="payment.action.end" var="labelSubmit"/>
                            <input name="submit" class="btn btn-primary my-2 my-sm-0" type="submit"
                                   value="${labelSubmit}"/>
                        </td>
                    </tr>
                </table>
            </form:form>
        </c:when>
        <c:otherwise>
            <div class="display"><spring:message code="rents.list.message.empty"/></div>
        </c:otherwise>
    </c:choose>
</div>

</body>
</html>
