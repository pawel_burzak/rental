<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>rental</title>
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/js/timer.js" />"></script>

</head>
<body onload="startTime(); setTime();">

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="collapse navbar-collapse" id="navbarColor01">

        <c:if test="${not empty failure}">
            <div class="alert alert-dismissible alert-danger mr-auto">
                <strong>${failure}</strong>
            </div>
        </c:if>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item" style="padding-right: 20px; font-size: 23px; font-weight: bold" id="clock"></li>
            <li class="nav-item navi_button">
                <a href="<c:url value='/main'/>">
                    <img type="button" class="home_button">
                </a>
            </li>
            <li class="nav-item navi_button">
                <c:url var="logoutUrl" value="/perform_logout"/>
                <form:form method="post" action="${logoutUrl}">
                    <input type="image" value="" class="logout_button"/>
                </form:form>
            </li>
        </ul>
    </div>
</nav>

<div class="form_find">
    <c:url var="findAction" value="/rent/findAccount"/>
    <form:form class="form-inline my-2 my-lg-0" method="post" modelAttribute="foundAccount" action="${findAction}">

    <div class="col-md-12">

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label style="line-height: 1.5">Znajdź klienta:</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <form:input class="input_form" path="phoneNumber" placeholder="Numer telefonu"/>
                    <label class="control-label"><form:errors path="phoneNumber" cssClass="error lb-sm"/></label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <spring:message code="action.find" var="labelSubmit"/>
                    <input name="submit" class="btn btn-primary my-2 my-sm-0" type="submit" value="${labelSubmit}"/>
                </div>
            </div>
        </div>
        </form:form>
    </div>

    <c:if test="${not empty account}">
        <div class="display">
            <c:out value="Znaleziony klient: ${account.firstName} ${account.lastName}"/>
        </div>
    </c:if>
</div>


<table class="form_rent">
    <c:choose>
    <c:when test="${!empty account}">
    <c:url value="/rent/saveRent" var="saveAction"/>
    <form:form method="post" modelAttribute="order" action="${saveAction}">
    <tr>
        <c:forEach var="rent" items="${order.rentsDTO}" varStatus="i">
        <c:if test="${not i.first and i.index % 2 == 0}">
    </tr>
    <tr>
        </c:if>
        <input type="hidden" name="rentsDTO[${i.index}].equipmentId" value="${rent.equipmentId}">
        <td style="padding: 5px">${rent.equipmentName}</td>
        <td style="padding: 5px"><input type="number" style="width: 30px; text-align: center" name="rentsDTO[${i.index}].amount" value="0"/>
        </td>
        </c:forEach>
    </tr>
    <form:hidden path="accountId"/>
</table>
<table class="form_rent" style="width: 300px">
    <form:errors path="rentStart" cssClass="error"/>
    <tr>
        <td><form:input path="rentStart" type="time" id="time"/></td>
        <td>
            <spring:message code="action.rent" var="labelSubmit"/>
            <input name="submit" class="btn btn-primary my-2 my-sm-0" type="submit" value="${labelSubmit}"/>
        </td>
    </tr>
</table>

</form:form>
</c:when>
</c:choose>

</body>
</html>
