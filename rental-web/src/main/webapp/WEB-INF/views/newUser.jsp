<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>rental</title>

    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/js/timer.js" />"></script>
    <script src="<c:url value="/resources/js/confirm.js" />"></script>

</head>

<body onload="startTime()">

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="collapse navbar-collapse" id="navbarColor01">

        <c:url var="findAction" value="/account/find"/>
        <form:form class="form-inline my-2 my-lg-0" method="post" action="${findAction}" modelAttribute="findAccounts">
            <form:input path="firstName" class="form-control-search ml-sm-2" placeholder="Imię"/>
            <form:input path="lastName" class="form-control-search ml-sm-2" placeholder="Nazwisko"/>
            <spring:message code="action.find" var="labelSubmit"/>
            <input type="submit" class="btn btn-primary my-2 my-sm-0" name="submit" value="${labelSubmit}">
        </form:form>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item" style="padding-right: 20px; font-size: 23px; font-weight: bold" id="clock"></li>
            <li class="nav-item navi_button">
                <a href="<c:url value='/main'/>">
                    <img type="button" class="home_button">
                </a>
            </li>
            <li class="nav-item navi_button">
                <c:url var="logoutUrl" value="/perform_logout"/>
                <form:form method="post" action="${logoutUrl}">
                    <input type="image" value="" class="logout_button"/>
                </form:form>
            </li>
        </ul>
    </div>
</nav>

<div style="margin-top: 100px">
    <c:choose>
        <c:when test="${!empty foundAccounts}">
            <table class="table table-striped table-hover table-bordered">
                <thead class="thead-blue">
                <tr>
                    <th><spring:message code="label.firstAndLastName"/></th>
                    <th><spring:message code="label.phoneNumber"/></th>
                    <th><spring:message code="label.email"/></th>
                    <th><spring:message code="label.streetAddress"/></th>
                    <th><spring:message code="label.postalCode"/></th>
                    <th><spring:message code="label.city"/></th>
                    <th colspan="2"></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="account" items="${foundAccounts}">
                    <tr>
                        <td>${account.firstName} ${account.lastName}</td>
                        <td>${account.phoneNumber}</td>
                        <td>${account.email}</td>
                        <td>${account.address.streetAddress}</td>
                        <td>${account.address.postalCode}</td>
                        <td>${account.address.city}</td>
                        <td>
                            <a href="<c:url value='/account/edit/${account.id}'/>">
                                <spring:message code="action.edit"/>
                            </a>
                        </td>
                        <td>
                            <a href="<c:url value='/account/delete/${account.id}'/>" onclick="return confirmDeleteAccount()">
                                <spring:message code="action.delete"/>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:when>
    </c:choose>
</div>

<div class="form_newUser">
    <c:url var="saveAction" value="/account/save"/>
    <form:form method="post" modelAttribute="account" action="${saveAction}">

        <form:hidden path="id"/>
        <div class="col-md-12">
            <label class="control-label"><spring:message code="label.account"/></label>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <form:input class="input_form" path="firstName" placeholder="Imię"/>
                        <label class="control-label"><form:errors path="firstName" cssClass="error lb-sm"/></label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <form:input class="input_form" path="lastName" placeholder="Nazwisko"/>
                        <label class="control-label"><form:errors path="lastName" cssClass="error lb-sm"/></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <form:input class="input_form" path="phoneNumber" placeholder="Numer telefonu"/>
                        <label class="control-label"><form:errors path="phoneNumber" cssClass="error lb-sm"/></label>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <form:input class="input_form" path="email" placeholder="Email"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <form:input class="input_form" path="address.postalCode" placeholder="Kod pocztowy"/>
                        <label class="control-label"><form:errors path="address.postalCode"
                                                                  cssClass="error lb-sm"/></label>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <form:input class="input_form" path="address.city" placeholder="Miasto"/>
                        <label class="control-label"><form:errors path="address.city" cssClass="error lb-sm"/></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <form:input class="input_form" path="address.streetAddress" placeholder="Ulica i numer domu"/>
                        <label class="control-label"><form:errors path="address.streetAddress"
                                                                  cssClass="error lb-sm"/></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <spring:message code="action.save" var="labelSubmit"/>
                        <input name="submit" class="input_form btn-primary" type="submit" value="${labelSubmit}"/>
                    </div>
                </div>
            </div>
        </div>
    </form:form>
</div>

</body>
</html>