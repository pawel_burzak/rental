<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>rental</title>

    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
    <script src="<c:url value="/resources/js/timer.js" />"></script>
    <script src="<c:url value="/resources/js/confirm.js" />"></script>

</head>
<body onload="startTime()">

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="collapse navbar-collapse" id="navbarColor01">
        <c:if test="${not empty success}">
            <div class="alert alert-dismissible alert-success mr-auto">
                <strong>${success}</strong>
            </div>
        </c:if>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item" style="padding-right: 20px; font-size: 23px; font-weight: bold" id="clock"></li>
            <li class="nav-item navi_button">
                <a href="<c:url value='/main'/>">
                    <img type="button" class="home_button">
                </a>
            </li>
            <li class="nav-item navi_button">
                <c:url var="logoutUrl" value="/perform_logout"/>
                <form:form method="post" action="${logoutUrl}">
                    <input type="image" value="" class="logout_button"/>
                </form:form>
            </li>
        </ul>
    </div>
</nav>


<div class="table_new_equipment">
    <c:url var="saveAction" value="/equipment/save"/>
    <form:form method="post" modelAttribute="equipment" action="${saveAction}">
        <form:hidden path="id"/>
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <form:label path="name">
                            <spring:message code="equipment.label.name"/>
                        </form:label>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="form-group">
                        <form:input class="input_form" path="name"/>
                        <label class="control-label"><form:errors path="name" cssClass="error lb-sm"/></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9">
                    <div class="form-group">
                        <form:label path="pricePerHour">
                            <spring:message code="equipment.label.pricePerHour"/>
                        </form:label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <form:input class="input_form" path="pricePerHour"/>
                        <label class="control-label"><form:errors path="pricePerHour" cssClass="error lb-sm"/></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-9">
                    <div class="form-group">
                        <form:label path="pricePerDay">
                            <spring:message code="equipment.label.pricePerDay"/>
                        </form:label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <form:input class="input_form" path="pricePerDay"/>
                        <label class="control-label"><form:errors path="pricePerDay" cssClass="error lb-sm"/></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <spring:message code="action.save" var="labelSubmit"/>
                        <input name="submit" class="input_form btn-primary" type="submit" value="${labelSubmit}"/>
                    </div>
                </div>
            </div>
        </div>
    </form:form>
</div>

<div class="table_all_equipment">
    <table class="table table-striped table-hover table-bordered">
        <thead class="thead-blue">
        <tr>
            <th rowspan="2" style="text-align:center;vertical-align:middle;padding:0">Nazwa</th>
            <th colspan="2">Cena</th>
            <th colspan="2" rowspan="2"></th>
        </tr>
        <tr>
            <th>za godzinę</th>
            <th>za dzień</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="equipment" items="${equipmentList}">
            <tr>
                <td>${equipment.name}</td>
                <td>${equipment.pricePerHour}</td>
                <td>${equipment.pricePerDay}</td>
                <td>
                    <a href="<c:url value='/equipment/edit/${equipment.id}'/>">
                        <spring:message code="action.edit"/>
                    </a>
                </td>
                <td>
                    <a href="<c:url value='/equipment/delete/${equipment.id}'/>" onclick="return confirmDeleteEquipment()">
                        <spring:message code="action.delete"/>
                    </a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>
