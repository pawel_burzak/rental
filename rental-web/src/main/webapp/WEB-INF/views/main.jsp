<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>rental</title>

        <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">
        <script src="<c:url value="/resources/js/timer.js" />"></script>

    </head>
    <body onload="startTime()">

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <div class="collapse navbar-collapse" id="navbarColor01">
            <c:if test="${not empty success}">
                <div class="alert alert-dismissible alert-success mr-auto">
                    <strong>${success}</strong>
                </div>
            </c:if>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item" style="padding-right: 20px; font-size: 23px; font-weight: bold" id="clock"></li>
                <li class="nav-item navi_button">
                    <a href="<c:url value='/equipment'/>">
                        <img type="button" class="edit_button">
                    </a>
                </li>
                <li class="nav-item navi_button">
                    <c:url var="logoutUrl" value="/perform_logout"/>
                    <form:form method="post" action="${logoutUrl}">
                        <input type="image" value="" class="logout_button"/>
                    </form:form>
                </li>
            </ul>
        </div>
    </nav>

    <div class="main">
        <div class="main_button">
            <a href="<c:url value='/account'/>">
                <img type="button" class="account_button">
            </a>
        </div>
        <div class="main_button">
            <a href="<c:url value='/rent'/>">
                <img type="button" class="rent_button">
            </a>
        </div>
        <div class="main_button">
            <a href="<c:url value='/payment'/>">
                <img type="button" class="payment_button">
            </a>
        </div>
        </div>

    </body>
</html>