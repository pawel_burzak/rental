package pl.sda.rental.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.sda.rental.domain.entity.Account;
import pl.sda.rental.service.account.service.AccountService;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class AccountControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private AccountController accountController;

    @Mock
    private AccountService accountService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(accountController)
                .build();
    }

    @Test
    public void testMainPage() throws Exception {

        mockMvc.perform(get("/account"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("account", instanceOf(Account.class)))
                .andExpect(model().attribute("findAccounts", instanceOf(Account.class)))
                .andExpect(view().name("newUser"));
    }

    @Test
    public void testSaveAccountSuccess() throws Exception {

        when(accountService.create(any(Account.class))).thenReturn(Long.valueOf(1));

        mockMvc.perform(post("/account/save"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/account"));
    }

    @Test
    public void testFindAccountSuccess() throws Exception{

        mockMvc.perform(post("/account/find"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("foundAccounts", hasSize(0)))
                .andExpect(model().attribute("account", instanceOf(Account.class)))
                .andExpect(view().name("newUser"));
    }

    @Test
    public void testEditAccountSuccess() throws Exception{
        Long id = 1L;

        when(accountService.findById(id)).thenReturn(new Account());

        mockMvc.perform(get("/account/edit/" + id))
                .andExpect(status().isOk())
                .andExpect(model().attribute("findAccounts", instanceOf(Account.class)))
                .andExpect(model().attribute("account", instanceOf(Account.class)))
                .andExpect(view().name("newUser"));
    }

    @Test
    public void testDeleteAccountSuccess() throws Exception {
        Long id = 1L;

        doNothing().when(accountService).delete(id);

        mockMvc.perform(get("/account/delete/" + id))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/account"));

        verify(accountService, times(1)).delete(id);
    }

}